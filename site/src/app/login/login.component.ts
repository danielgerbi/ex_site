import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  showModal: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onShowModal() {
    this.showModal = !this.showModal;
  }
}
