import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  sliderImageIndex: number = 0;

  constructor() { }

  ngOnInit() {
  }

  setSliderImage(index: number) {
    this.sliderImageIndex = index;
  }
}
