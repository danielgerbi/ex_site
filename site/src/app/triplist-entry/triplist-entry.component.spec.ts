import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TriplistEntryComponent } from './triplist-entry.component';

describe('TriplistEntryComponent', () => {
  let component: TriplistEntryComponent;
  let fixture: ComponentFixture<TriplistEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TriplistEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TriplistEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
