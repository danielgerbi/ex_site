import { Component, OnInit, Input } from '@angular/core';
import { Trip } from '../trip/trip';

@Component({
  selector: 'app-triplist-entry',
  templateUrl: './triplist-entry.component.html',
  styleUrls: ['./triplist-entry.component.css']
})
export class TriplistEntryComponent implements OnInit {
  @Input() trip: Trip;
  expanded: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  expand() {
    this.expanded = !this.expanded;
  }
}
