export class Trip {
    title: string;
    image: string;
    description: string;
}