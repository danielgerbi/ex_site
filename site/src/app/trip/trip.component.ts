import { Component, OnInit } from '@angular/core';
import { Trip } from "./trip";
import { TRIPS } from "./mock-trips";

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {
  firstPair: Trip[];
  secondPair: Trip[];

  constructor() { }

  ngOnInit() {
    this.firstPair = TRIPS.slice(0, 2);
    this.secondPair = TRIPS.slice(2, 4);
  }

}
