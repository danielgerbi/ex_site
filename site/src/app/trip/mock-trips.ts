import { Trip } from "./trip";

export const TRIPS: Trip[] = [
    { title: "Greece", description: "Greece  has an amazing sea, and is also cheap", image: "https://www.zingarate.com/pictures/2018/07/04/mare-grecia.jpeg"},
    { title: "Britain", description: "Britain is the most winderfull place in Europe", image: "https://www.myenglishschool.it/magazine/wp-content/uploads/2016/10/Vita-a-Londra-1.jpg"},
    { title: "New Zealand", description: "New zealand is a magic place!", image: "https://www.nationsonline.org/gallery/New_Zealand/Lake-Tekapo-New-Zealand.jpg"},
    { title: "Australia", description: "where you feel the real face of Nature", image: "https://www.trafalgar.com/~/media/images/home/destinations/south-pacific/australia/2016-licensed-images/australia-sturtnationalpark-kangaroo-2016-r-520654993-australiaswildlife.jpg?la=en&h=450&w=450&mw=450"},
]