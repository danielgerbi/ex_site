import { Component, OnInit } from '@angular/core';
import { Trip } from '../trip/trip';
import { TRIPS } from '../trip/mock-trips';

@Component({
  selector: 'app-triplist',
  templateUrl: './triplist.component.html',
  styleUrls: ['./triplist.component.css']
})
export class TriplistComponent implements OnInit {
  userTrips: Trip[];

  constructor() { }

  ngOnInit() {
    this.userTrips = TRIPS;
  }

  expand(index:number) {

  }
}
