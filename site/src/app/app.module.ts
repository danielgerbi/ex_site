import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TripComponent } from './trip/trip.component';
import { SliderComponent } from './slider/slider.component';
import { LoginComponent } from './login/login.component';
import { TriplistComponent } from './triplist/triplist.component';
import { TriplistEntryComponent } from './triplist-entry/triplist-entry.component';
import { AppRoutingModule } from './/app-routing.module';
import { MainViewComponent } from './main-view/main-view.component';

@NgModule({
  declarations: [
    AppComponent,
    TripComponent,
    SliderComponent,
    LoginComponent,
    TriplistComponent,
    TriplistEntryComponent,
    MainViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
